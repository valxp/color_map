
package color_map;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ColorMap {

    public ColorMap() {

    }

    public static void main(String[] args) {
        if (args == null || args.length != 1) {
            System.out.println("Error : must specify a map file as argument");
            return;
        }
        FileReader file = null;
        try {
            file = new FileReader(args[0]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Parsing " + args[0]);
        long before = System.currentTimeMillis();
        Map map = new Map(file);
        System.out.println("Done : " + (System.currentTimeMillis() - before) + "ms");
        System.out.println(Country.getClassCount() + " Countries in map");

        System.out.println("Assigning colors");
        before = System.currentTimeMillis();
        map.assignColors();
        System.out.println("Done : " + (System.currentTimeMillis() - before) + "ms");

        System.out.println("Outputing");
        map.outputTo(System.out);
        System.out.println("\nDone");
    }

}
