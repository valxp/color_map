
package color_map;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import color_map.Country.CountryListener;

public class Map implements CountryListener {

    public static interface MapListener {
        public void onMapChange();
    }

    private int mMapWidth = 0;
    private int mMapHeight = 0;
    // Storing every elements in a list additionally to the graph to be able to
    // recreate the map easily
    Vector<Country> mMap;

    private MapListener mListener;

    public void setListener(MapListener listener) {
        mListener = listener;
    }

    public Map(InputStreamReader input) {
        try {
            List<Character> charMap = new ArrayList<Character>();
            char c = 0;
            while ((c = (char) input.read()) != (char) -1) {
                System.out.print((char) c);
                // NewLine
                if (c == '\n') {
                    mMapHeight++;
                    continue;
                }
                if (mMapHeight == 0)
                    mMapWidth++;
                charMap.add(c);
            }
            // If the map is not a rectangle, abort
            if (charMap.size() != mMapHeight * mMapWidth)
                return;
            mMap = new Vector<Country>(charMap.size());
            mMap.setSize(mMap.capacity());
            for (int y = 0; y < mMapHeight; ++y) {
                for (int x = 0; x < mMapWidth; ++x) {
                    buildGraph(charMap, x, y);
                }
            }
            for (Country cty : mMap) {
                cty.resolvePositions(mMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int XYtoW(int x, int y) {
        if (x < 0 || x >= mMapWidth || y < 0 || y >= mMapHeight)
            return -1;
        return (y * mMapWidth + x);
    }

    private void buildGraph(List<Character> charMap, int x, int y) {
        Character me = charMap.get(XYtoW(x, y));
        Country country = mMap.elementAt(XYtoW(x, y));

        int topPos, bottomPos, leftPos, rightPos;

        topPos = XYtoW(x, y - 1);
        bottomPos = XYtoW(x, y + 1);
        leftPos = XYtoW(x - 1, y);
        rightPos = XYtoW(x + 1, y);

        Country top, bottom, left, right;

        top = getCountryAt(charMap, topPos);
        bottom = getCountryAt(charMap, bottomPos);
        left = getCountryAt(charMap, leftPos);
        right = getCountryAt(charMap, rightPos);

        if (top != null && top.getName() == me)
            country = top;
        if (bottom != null && bottom.getName() == me)
            country = bottom;
        if (left != null && left.getName() == me)
            country = left;
        if (right != null && right.getName() == me)
            country = right;
        if (country == null) {
            country = new Country(me);
            country.setListener(this);
        }
        mMap.set(XYtoW(x, y), country);

        Character ctop, cbottom, cleft, cright;

        ctop = getCharAt(charMap, topPos);
        cbottom = getCharAt(charMap, bottomPos);
        cleft = getCharAt(charMap, leftPos);
        cright = getCharAt(charMap, rightPos);

        country.addPosition(topPos);
        country.addPosition(bottomPos);
        country.addPosition(leftPos);
        country.addPosition(rightPos);

        if (ctop != null && ctop.equals(me) && top == null) {
            buildGraph(charMap, x, y - 1);
        }
        if (cbottom != null && cbottom.equals(me) && bottom == null) {
            buildGraph(charMap, x, y + 1);
        }
        if (cleft != null && cleft.equals(me) && left == null) {
            buildGraph(charMap, x - 1, y);
        }
        if (cright != null && cright.equals(me) && right == null) {
            buildGraph(charMap, x + 1, y);
        }
    }

    private Character getCharAt(List<Character> charMap, int pos) {
        if (pos != -1)
            return charMap.get(pos);
        return null;
    }

    private Country getCountryAt(List<Character> charMap, int pos) {
        if (pos != -1)
            return mMap.elementAt(pos);
        return null;
    }

    public void assignColors() {
        // No need to loop through the whole map since the first element will
        // recursively resolve the color for all its adjacent elements
        if (mMap != null && mMap.size() > 0)
            mMap.get(0).assignColor();
    }

    public void outputTo(OutputStream out) {
        int i = 0;
        try {
            for (Country cty : mMap) {
                if (i % mMapWidth == 0)
                    out.write('\n');
                out.write(cty.getColor());
                ++i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getWidth() {
        return mMapWidth;
    }

    public int getHeight() {
        return mMapHeight;
    }

    public Country getAt(int x, int y) {
        return mMap.get(y * mMapWidth + x);
    }

    @Override
    public void onCountryChange() {
        if (mListener != null)
            mListener.onMapChange();
    }
    
    public boolean isOkay() {
        return mMap != null;
    }
}
