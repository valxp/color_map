package color_map;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Country {
	public interface CountryListener {
		public void onCountryChange();
	}

	private static int CLASSCOUNT = 0;
	private static final char[] COLORS = { 'R', 'G', 'B', 'Y' };
	private char mName;
	private List<Country> mAdjacents = new ArrayList<Country>();
	private List<Integer> mPos= new ArrayList<Integer>();
	private char mColor = 0;
	private CountryListener mListener;

	// Stores the last color tried.
	private int mLastForcedColor = 0;

	public Country(char name) {
		++CLASSCOUNT;
		mName = name;
	}

	public static int getClassCount() {
		return CLASSCOUNT;
	}

	public char getName() {
		return mName;
	}
	
	public void addPosition(int position) {
	    if (position == -1)
	        return;
	    mPos.add(position);
	}

	public void addAdjacent(Country adjacent) {
		// No need to add itself
		// No need to add if already present
		if (adjacent == null || adjacent == this
				|| mAdjacents.contains(adjacent))
			return;
		mAdjacents.add(adjacent);
	}
	
	public void resolvePositions(Vector<Country> countries) {
	    if (mPos == null)
	        return;
	    for (int pos : mPos) {
	        addAdjacent(countries.get(pos));
	    }
	    mPos = null;
	}

	public char getColor() {
		return mColor;
	}

	public boolean isColorSet() {
		return mColor != 0;
	}

	public void reAssignColor() {
		mColor = 0;
		if (mListener != null)
			mListener.onCountryChange();
		assignColor();
	}

	
	public void assignColor() {
		// Color already assigned
		if (isColorSet())
			return;
		List<Character> usedColors = new ArrayList<Character>();
		List<Country> toAssign = new ArrayList<Country>();

		for (Country adj : mAdjacents) {
			if (adj.isColorSet())
				usedColors.add(adj.getColor());
			else
				toAssign.add(adj);
		}
		int i;
		// Looping to find a color not used by adjacent country
		for (i = 0; i < COLORS.length && usedColors.contains(COLORS[i]); ++i)
			;
		// if all colors are used, pick one and force the adjacent countries
		// with this color to find another one
		// try a new color for each time this country blocks with no color to
		// try every possibilities
		if (i == COLORS.length) {
			mLastForcedColor = mLastForcedColor % COLORS.length;
			mColor = COLORS[mLastForcedColor++];
			if (mListener != null)
				mListener.onCountryChange();
			System.out.println(getName() + " Stuck ! Forcing " + mColor);
			for (Country adj : mAdjacents) {
				if (adj.getColor() == mColor)
					adj.reAssignColor();
			}
		} else {
			// Everything is fine, use this color
			mColor = COLORS[i];
			if (mListener != null)
				mListener.onCountryChange();
		}

		// assigning colors to adjacent countries
		for (Country adj : toAssign)
			adj.assignColor();
	}

	public void setListener(CountryListener listener) {
		mListener = listener;
	}
}
