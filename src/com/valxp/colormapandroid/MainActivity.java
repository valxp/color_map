package com.valxp.colormapandroid;

import java.io.FileReader;
import java.io.IOException;

import com.example.colormapandroid.R;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import color_map.Country;
import color_map.Map;

public class MainActivity extends Activity {
	private static final int PICK_FILE_CODE = 4242;
	TextView mSelectFile;
	TextView mStatus;
	MapView mMapView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initView();
	}

	private void initView() {
		mStatus = (TextView) findViewById(R.id.status);
		mMapView = (MapView) findViewById(R.id.mapView);
		mSelectFile = (TextView) findViewById(R.id.chooseMap);
		mSelectFile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("*/*");
				intent.addCategory(Intent.CATEGORY_OPENABLE);

				try {
					startActivityForResult(intent, PICK_FILE_CODE);
				} catch (ActivityNotFoundException e) {
					// No compatible file manager was found.
					Toast.makeText(
							MainActivity.this,
							"Please install a file manager (ie: es file explorer)",
							Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_FILE_CODE) {
			if (resultCode == RESULT_OK && data != null) {
				String filePath = null;
				Uri uri = data.getData();
				Cursor c = getContentResolver().query(uri,
						new String[] { MediaStore.MediaColumns.DATA }, null,
						null, null);
				if (c != null && c.moveToFirst()) {
					int id = c.getColumnIndex(Images.Media.DATA);
					if (id != -1) {
						filePath = c.getString(id);
					}
				}
				if (filePath != null && filePath.length() > 0) {
					mSelectFile.setText(filePath);
					loadMapFile(filePath);
				} else {
					showError("Error loading file");
				}
			}
		}
	}

	private void showMessage(String message) {
		findViewById(R.id.separator).setVisibility(View.VISIBLE);
		mStatus.setVisibility(View.VISIBLE);
		mStatus.setTextColor(getResources().getColor(R.color.black));
		mStatus.setText(message);
	}

	private void showError(String error) {
		findViewById(R.id.separator).setVisibility(View.VISIBLE);
		mStatus.setVisibility(View.VISIBLE);
		mStatus.setTextColor(getResources().getColor(R.color.red));
		mStatus.setText(error);
	}

	private void loadMapFile(String fileName) {
		new ParseMap().execute(fileName);
	}

	public class ParseMap extends AsyncTask<String, String, Map> {

		@Override
		protected Map doInBackground(String... params) {
			String filename = params[0];
			FileReader file = null;
			publishProgress("Loading file...");
			try {
				file = new FileReader(filename);
			} catch (Exception e) {
			    e.printStackTrace();
				return null;
			}

			publishProgress("Parsing file...");
			Map map = null;
			try {
				map = new Map(file);
			} catch (Exception e) {
			    e.printStackTrace();
				return null;
			}

			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (map != null && !map.isOkay())
			    return null;
			return map;
		}

		@Override
		protected void onPostExecute(Map result) {
			if (result == null) {
				showError("Error loading map");
			} else {
				showMessage("Map load success. " + Country.getClassCount()
						+ " countries loaded");
				mMapView.setVisibility(View.VISIBLE);
				mMapView.setMap(result);
				Assigncolors task = new Assigncolors();
				task.execute(result);
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			showMessage(values[0]);
		}
	}

	public class Assigncolors extends AsyncTask<Map, String, Boolean> {

		@Override
		protected Boolean doInBackground(Map... params) {
			publishProgress("Searching for colors");
			try {
			params[0].assignColors();
			} catch (Exception e) {
			    return false;
			}
			return true;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			showMessage(values[0]);
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
		    if (result)
    			showMessage("Success !");
		    else
		        showError("Ooops... Solving failed");
		}
	}

}
