
package com.valxp.colormapandroid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import color_map.Country;
import color_map.Map;
import color_map.Map.MapListener;

import com.example.colormapandroid.R;

public class MapView extends View implements MapListener {

    private Map mMap;
    private Paint mTextPaint = new Paint();
    private Paint mRectPaint = new Paint();
    private Rect mBounds = new Rect();

    public MapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapView(Context context) {
        super(context);
    }

    public void setMap(Map map) {
        mMap = map;
        map.setListener(this);
        invalidate();
    }

    
    // this function can be optimized a lot. By not re-drawing the whole map every time
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(getResources().getColor(android.R.color.white));
        if (mMap == null)
            return;
        int mapWidth = mMap.getWidth();
        int mapHeight = mMap.getHeight();

        int cvWidth = canvas.getWidth();
        int cvHeight = canvas.getHeight();

        int charSizeW = cvWidth / mapWidth;
        int charSizeH = cvHeight / mapHeight;

        mTextPaint.setTextSize(Math.min(charSizeW, charSizeH));

        for (int y = 0; y < mapHeight; ++y) {
            for (int x = 0; x < mapWidth; ++x) {
                Country country = mMap.getAt(x, y);
                if (country == null)
                    continue;
                // Can optimize with a map here
                switch (country.getColor()) {
                    case 'R':
                        mRectPaint.setColor(getResources().getColor(R.color.red));
                        break;
                    case 'G':
                        mRectPaint.setColor(getResources().getColor(R.color.green));
                        break;
                    case 'B':
                        mRectPaint.setColor(getResources().getColor(R.color.blue));
                        break;
                    case 'Y':
                        mRectPaint.setColor(getResources().getColor(R.color.yellow));
                        break;
                    default:
                        mRectPaint.setColor(getResources().getColor(R.color.white));
                        break;
                }
                mBounds.left = x * charSizeW;
                mBounds.right = (x + 1) * charSizeW;
                mBounds.top = y * charSizeH;
                mBounds.bottom = (y + 1) * charSizeH;
                canvas.drawRect(mBounds, mRectPaint);

                mTextPaint.getTextBounds("" + country, 0, 1, mBounds);
                canvas.drawText("" + country.getName(), x * charSizeW + (int) (.5 * mBounds.right), y * charSizeH
                        - (int) (1.5 * mBounds.top), mTextPaint);

            }
        }
    }

    // called from a thread
    @Override
    public void onMapChange() {
        post(new Runnable() {

            @Override
            public void run() {
                invalidate();
            }
        });
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
